Install instruction for Amazon AWS
=====================

* create new instance from default AMI "ubuntu 14.04"
* ssh to new instance
* update package manager ``sudo apt-get update``
* install git: ``sudo apt-get install git``
* clone this repository with ``git clone URL`` and enter the new directory

as selenium hub
---
 * start install script for selenium hub ``./installAsSeleniumHub.sh``

or

as selenium node
---
 * make a copy of node configuration: ``cp config/node_config.dist.json config/node_config.json``
 * fill in the desired IP or DNS of the selenium hub by changing the value ``"hubHost":"x.x.x.x"`` in ``config/node_config.json``
 * start install script for selenium node ``./installAsSeleniumNode.sh``
 
 
Install instructions for local environment
===================
* make a copy of node configuration: ``cp config/node_config.dist.json config/node_config.json``
* fill in the desired IP for selenium hub by changing the value ``"hubHost":"x.x.x.x"`` in ``config/node_config.json``
* make a copy of vagrant configuration: ``cp config/vagrant_config.dist.yaml config/vagrant_config.yaml``
* optional: change vagrant configuration for cpu, ram, number of nodes , etc. in file ``config/vagrant_config.yaml``
* execute ``vagrant up``
# load "vagrant_config.yaml"
require 'yaml'
if File.file?("config/vagrant_config.yaml")
    vconfig = YAML::load_file("config/vagrant_config.yaml")
else
    vconfig = YAML::load_file("config/vagrant_config.dist.yaml")
end

# gather host info and save it to "hostinfo.txt"
# username in ruby 2.x
username = ENV['USERNAME']
if username.nil?
    # username in ruby 1.x
    username =  ENV['USER']
end
require 'socket'
usernameAtHost = username + "@" + Socket.gethostbyname(Socket.gethostname).first
File.write('config/hostinfo.txt', usernameAtHost)

# Do it ...
Vagrant.configure("2") do |config|

    # fix for annoying "stdin: is not a tty":
    config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"

    if vconfig['HUB_COUNT'] == 1
        config.vm.define :hub do |hub_config|
            hub_config.vm.box = vconfig['BASE_BOX']
            hub_config.vm.hostname = "aws-selenium-hub"
            hub_config.vm.provider :virtualbox do |vb|
                vb.customize ["modifyvm", :id, "--name", "aws-selenium-hub"]
                vb.customize ["modifyvm", :id, "--memory", vconfig['VIRTUAL_BOX_MEMORY']]
                vb.customize ["modifyvm", :id, "--cpus", vconfig['VIRTUAL_BOX_CPUS']]
            end
            hub_config.vm.network "public_network", bridge: "eth0"
            hub_config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", disabled: true
            hub_config.vm.network :forwarded_port, guest: 22, host: 2440, auto_correct: true
            hub_config.vm.provision :shell, path: "resources/vagrant_bootstrap_hub.sh", privileged: false
            hub_config.vm.synced_folder ".", "/home/vagrant/synced"
            hub_config.vm.provision :shell, :inline => "/etc/init.d/selenium-hub-service start", run: "always"
            hub_config.vm.provision :shell, :inline => "/etc/rc.local", run: "always"

        end
    end

    if vconfig['NODES_COUNT'] >= 1
        config.vm.define :node1 do |node1_config|
            node1_config.vm.box = vconfig['BASE_BOX']
            node1_config.vm.hostname = "node1"
            node1_config.vm.provider :virtualbox do |vb|
                vb.customize ["modifyvm", :id, "--name", "#{usernameAtHost} node1"]
                vb.customize ["modifyvm", :id, "--memory", vconfig['VIRTUAL_BOX_MEMORY']]
                vb.customize ["modifyvm", :id, "--cpus", vconfig['VIRTUAL_BOX_CPUS']]
            end
            node1_config.vm.network "public_network", bridge: "eth0"
            node1_config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", disabled: true
            node1_config.vm.network :forwarded_port, guest: 22, host: 2441, auto_correct: true
            node1_config.vm.provision :shell, path: "resources/vagrant_bootstrap_node.sh", privileged: false
            node1_config.vm.synced_folder ".", "/home/vagrant/synced"
            node1_config.vm.provision :shell, :inline => "/etc/init.d/selenium-node-service start", run: "always"
            node1_config.vm.provision :shell, :inline => "/etc/rc.local", run: "always"
        end
    end

    if vconfig['NODES_COUNT'] >= 2
        config.vm.define :node2 do |node2_config|
            node2_config.vm.box = vconfig['BASE_BOX']
            node2_config.vm.hostname = "node2"
            node2_config.vm.provider :virtualbox do |vb|
                vb.customize ["modifyvm", :id, "--name", "#{usernameAtHost} node2"]
                vb.customize ["modifyvm", :id, "--memory", vconfig['VIRTUAL_BOX_MEMORY']]
                vb.customize ["modifyvm", :id, "--cpus", vconfig['VIRTUAL_BOX_CPUS']]
            end
            node2_config.vm.network "public_network", bridge: "eth0"
            node2_config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", disabled: true
            node2_config.vm.network :forwarded_port, guest: 22, host: 2442, auto_correct: true
            node2_config.vm.provision :shell, path: "resources/vagrant_bootstrap_node.sh", privileged: false
            node2_config.vm.synced_folder ".", "/home/vagrant/synced"
            node2_config.vm.provision :shell, :inline => "/etc/init.d/selenium-node-service start", run: "always"
            node2_config.vm.provision :shell, :inline => "/etc/rc.local", run: "always"
        end
    end

    if vconfig['NODES_COUNT'] >= 3
        config.vm.define :node3 do |node3_config|
            node3_config.vm.box = vconfig['BASE_BOX']
            node3_config.vm.hostname = "node3"
            node3_config.vm.provider :virtualbox do |vb|
                vb.customize ["modifyvm", :id, "--name", "#{usernameAtHost} node3"]
                vb.customize ["modifyvm", :id, "--memory", vconfig['VIRTUAL_BOX_MEMORY']]
                vb.customize ["modifyvm", :id, "--cpus", vconfig['VIRTUAL_BOX_CPUS']]
            end
            node3_config.vm.network "public_network", bridge: "eth0"
            node3_config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", disabled: true
            node3_config.vm.network :forwarded_port, guest: 22, host: 2443, auto_correct: true
            node3_config.vm.provision :shell, path: "resources/vagrant_bootstrap_node.sh", privileged: false
            node3_config.vm.synced_folder ".", "/home/vagrant/synced"
            node3_config.vm.provision :shell, :inline => "/etc/init.d/selenium-node-service start", run: "always"
            node3_config.vm.provision :shell, :inline => "/etc/rc.local", run: "always"
        end
    end

    if vconfig['NODES_COUNT'] >= 4
        config.vm.define :node4 do |node4_config|
            node4_config.vm.box = vconfig['BASE_BOX']
            node4_config.vm.hostname = "node4"
            node4_config.vm.provider :virtualbox do |vb|
                vb.customize ["modifyvm", :id, "--name", "#{usernameAtHost} node4"]
                vb.customize ["modifyvm", :id, "--memory", vconfig['VIRTUAL_BOX_MEMORY']]
                vb.customize ["modifyvm", :id, "--cpus", vconfig['VIRTUAL_BOX_CPUS']]
            end
            node4_config.vm.network "public_network", bridge: "eth0"
            node4_config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", disabled: true
            node4_config.vm.network :forwarded_port, guest: 22, host: 2444, auto_correct: true
            node4_config.vm.provision :shell, path: "resources/vagrant_bootstrap_node.sh", privileged: false
            node4_config.vm.synced_folder ".", "/home/vagrant/synced"
            node4_config.vm.provision :shell, :inline => "/etc/init.d/selenium-node-service start", run: "always"
            node4_config.vm.provision :shell, :inline => "/etc/rc.local", run: "always"
        end
    end
end
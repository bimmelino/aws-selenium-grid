#!/bin/bash
# config
firefox_version_to_install="47.0.1"; # avaiable candidates, see: http://ftp.mozilla.org/pub/firefox/releases

# install common stuff first
source resources/installCommon.sh

# install fonts
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections
sudo apt-get --yes install msttcorefonts
sudo apt-get --yes install xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic

# install xvfb
sudo apt-get --yes install xvfb

# install x11 core
sudo apt-get --yes install xserver-xorg-core

# install firefox
[ -d /opt ] || sudo mkdir -m  755 /opt
cd /opt
if [ `uname -m` = x86_64 ]
then
    #sudo wget -N -q http://ftp.mozilla.org/pub/mozilla.org/firefox/nightly/${firefox_version_to_install}-candidates/build1/linux-x86_64/de/firefox-${firefox_version_to_install}.tar.bz2
    sudo wget -N -q http://ftp.mozilla.org/pub/firefox/releases/${firefox_version_to_install}/linux-x86_64/de/firefox-${firefox_version_to_install}.tar.bz2
else
    sudo wget -N -q http://ftp.mozilla.org/pub/firefox/releases/${firefox_version_to_install}/linux-i68/de/firefox-${firefox_version_to_install}.tar.bz2
fi
sudo tar xjf firefox-${firefox_version_to_install}.tar.bz2
sudo apt-get --yes purge firefox # removes firefox; comment out if definitely uninstalled
sudo rm -f /usr/bin/firefox
sudo ln -s /opt/firefox/firefox /usr/bin/firefox
sudo apt-get --yes install libpango-1.0-0
sudo apt-get --yes install libasound2
sudo apt-get --yes install libgtk-3.0

# copy selenium node config (.json)
sudo rm -f ${SELENIUM_WORKSPACE}/node_config.json
if [ -f ${THIS_SCRIPT_PATH}/config/node_config.json ] ; then
    sudo cp ${THIS_SCRIPT_PATH}/config/node_config.json ${SELENIUM_WORKSPACE}/node_config.json
else
    sudo cp ${THIS_SCRIPT_PATH}/config/node_config.dist.json ${SELENIUM_WORKSPACE}/node_config.json
fi
MY_INFO="$HOSTNAME `cat ${THIS_SCRIPT_PATH}/config/hostinfo.txt`"
sed -i -e "s/unknown owner/${MY_INFO}/g" ${SELENIUM_WORKSPACE}/node_config.json

# link init script for selenium node
sudo rm -f /etc/init.d/selenium-node-service
sudo cp ${THIS_SCRIPT_PATH}/resources/selenium-node-service /etc/init.d/selenium-node-service

# make it executable
sudo chmod +x ${THIS_SCRIPT_PATH}/resources/selenium-node-service

# install the service startup
sudo update-rc.d selenium-node-service defaults

# install htop
sudo apt-get --yes install htop

# install shellinabox
sudo apt-get --yes install shellinabox
sudo service shellinabox stop
sudo update-rc.d -f shellinabox remove

# install x11vnc server
sudo apt-get --yes install x11vnc
sudo cp ${THIS_SCRIPT_PATH}/resources/vncpasswd /selenium-workspace/vncpasswd
sudo cp -R ${THIS_SCRIPT_PATH}/resources/vncviewer /selenium-workspace/vncviewer

# add "htop_app", "nodelog_app" and "x11vnc" to boot up routine
sudo rm -f /etc/rc.local
sudo cp ${THIS_SCRIPT_PATH}/resources/rc.local /etc/rc.local
sudo chmod +x /etc/rc.local

# cloud only: install crontab to shutdown if no firefox is running (Cloud only)
if [ ${USER} != "vagrant" ] ; then
    sudo rm -f /selenium-workspace/cron_shutdown_inactive_node.sh
    sudo cp ${THIS_SCRIPT_PATH}/resources/cron_shutdown_inactive_node.sh /selenium-workspace/cron_shutdown_inactive_node.sh
    sudo chmod +x /selenium-workspace/cron_shutdown_inactive_node.sh
    (crontab -l ; echo '* * * * * sudo sh -e /selenium-workspace/cron_shutdown_inactive_node.sh') | crontab
fi

# cloud only: enable swap to make test execution more stable, especially on instances with low RAM
if [ ${USER} != "vagrant" ] ; then
    sudo dd if=/dev/zero of=/var/swapfile bs=1M count=2048 &&
    sudo chmod 600 /var/swapfile &&
    sudo mkswap /var/swapfile &&
    echo /var/swapfile none swap defaults 0 0 | sudo tee -a /etc/fstab &&
    sudo swapon -a
fi

# install crontab to kill all firefox browsers older than 15 minutes
sudo rm -f /selenium-workspace/cron_kill_hanging_firefox.sh
sudo cp ${THIS_SCRIPT_PATH}/resources/cron_kill_hanging_firefox.sh /selenium-workspace/cron_kill_hanging_firefox.sh
sudo chmod +x /selenium-workspace/cron_kill_hanging_firefox.sh
(crontab -l ; echo '* * * * * sudo sh -e /selenium-workspace/cron_kill_hanging_firefox.sh') | crontab

# install crontab to restart at 3 a.m. (for better stability)
if [ ${USER} = "vagrant" ] ; then
    (crontab -l ; echo '0 3 * * * sudo reboot now') | crontab
fi

# install crontab to restart "Xvfb" in case it has crashed (for better stability)
(crontab -l ; echo '* * * * * ps -C Xvfb | grep -q Xvfb || exec Xvfb :0 -screen 0 1024x768x24 &') | crontab

# start the selenium-hub-service
sudo /etc/init.d/selenium-node-service start


#!/bin/bash

source resources/installCommon.sh

# link init script for selenium hub
sudo rm -f /etc/init.d/selenium-hub-service
sudo cp ${THIS_SCRIPT_PATH}/resources/selenium-hub-service /etc/init.d/selenium-hub-service

# install cron to shutdown hub when no activity within last 15 minutes
if [ ${USER} != "vagrant" ] ; then
    sudo rm -f /selenium-workspace/cron_shutdown_inactive_hub.sh
    sudo cp ${THIS_SCRIPT_PATH}/resources/cron_shutdown_inactive_hub.sh /selenium-workspace/cron_shutdown_inactive_hub.sh
    sudo chmod +x /selenium-workspace/cron_shutdown_inactive_hub.sh
    (crontab -l ; echo '* * * * * sudo sh -e /selenium-workspace/cron_shutdown_inactive_hub.sh') | crontab
fi

# make it executable
sudo chmod +x /etc/init.d/selenium-hub-service

# install the service startup
sudo update-rc.d selenium-hub-service defaults

# start the selenium-hub-service
sudo /etc/init.d/selenium-hub-service start
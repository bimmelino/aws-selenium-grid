# installation of selenium standalone + xvfb + firefox (ubuntu preferred)

sudo apt-get update
sudo apt-get --yes install openjdk-7-jre-headless
sudo apt-get --yes install msttcorefonts
sudo apt-get --yes install xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic
sudo apt-get --yes install xvfb

# newest firefox version might be incompatible with selenium server. if so, edit the selenium server version to the newest
sudo apt-get install firefox
wget -N -q http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar

Xvfb :0 -screen 0 1024x768x24 2>/dev/null & export DISPLAY=:0;
java -jar selenium-server-standalone-2.53.0.jar -port 8444



# restart xvfb in case it crashed
# * * * * * ps -C Xvfb | grep -q Xvfb || exec Xvfb :0 -screen 0 1024x768x24 &

# kill zombie firefox instances
# * * * * * killall --older-than 15m firefox;

# reboot every morning
# 0 3 * * * sudo reboot now

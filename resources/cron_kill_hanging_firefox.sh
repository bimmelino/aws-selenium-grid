#!/bin/sh

# kill all firefox browsers older than 15 minutes
killall --older-than 15m firefox;
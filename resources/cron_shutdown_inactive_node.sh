#!/bin/sh
upSeconds="$(cat /proc/uptime | grep -o '^[0-9]\+')"
upMins=$((${upSeconds} / 60))
lastModification=`expr $(date +%s) - $(date +%s -r /selenium-workspace/node.log)`

if [ -z "$(pgrep firefox)" ] ; then                         # kein Firefox aktiv
    if [ "${upMins}" -gt "55" ] ; then                      # mindestens 55 Minuten Uptime
        cat /selenium-workspace/node.log | grep -q "Couldn't register this node"
        if [ "$?" -eq "0" ] || [ "${lastModification}" -gt "900" ]; then       # Hub nicht erreichbar laut Log ODER keine Modifikation am Log der Selenium-Node in den letzten 15 Minuten
            sudo shutdown -h -f now Reason: There is no active selenium session.;
        fi
    fi
fi
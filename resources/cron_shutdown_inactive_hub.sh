#!/bin/sh

SELENIUM_WORKSPACE=/selenium-workspace
HUB_LOG=${SELENIUM_WORKSPACE}/hub.log

upSeconds="$(cat /proc/uptime | grep -o '^[0-9]\+')"
upMins=$((${upSeconds} / 60))

lastModification=`expr $(date +%s) - $(date +%s -r $HUB_LOG)`

if [ "${upMins}" -gt "55" ] ; then              # mindestens 55 Minuten Uptime
    if [ "$lastModification" -gt 960 ] ; then   # keine Modifikation am Log des Selenium-Hub in den letzten 16 Minuten
        sudo shutdown -h -f now Reason: There is no selenium hub activity in the last 16 minutes.;
    fi
fi
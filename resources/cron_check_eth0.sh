#!/bin/sh

ping -c 1 www.google.de >/dev/null

if [ $? -ne 0 ] ; then
    echo "Network is broken. Restarting eth0 ..."
    sudo ifdown eth0
    sudo ifup eth0
fi

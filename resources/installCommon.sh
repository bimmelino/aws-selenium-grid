#!/bin/sh

# config
SELENIUM_VERSION_SHORT="2.53"
SELENIUM_VERSION_LONG="2.53.1"

# prepare variables
SELENIUM_USER=${USER}
SELENIUM_WORKSPACE="/selenium-workspace"
THIS_SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

# fix for annoying message: "dpkg-preconfigure: unable to re-open stdin: No such file or directory"
export DEBIAN_FRONTEND=noninteractive


# fix for annoying message: "Could not get lock /var/lib/apt/lists/lock - open (11: Resource temporarily unavailable)"
# sudo rm -rf /var/lib/apt/lists/*

# update apt-get packages
sudo apt-get update

# install java
sudo apt-get install --yes default-jre

# create workspace dir
sudo rm -rf ${SELENIUM_WORKSPACE}
sudo mkdir -p ${SELENIUM_WORKSPACE}
sudo chown ${SELENIUM_USER}:${SELENIUM_USER} ${SELENIUM_WORKSPACE}
echo ${SELENIUM_USER} > ${SELENIUM_WORKSPACE}/seleniumuser.txt

# download selenium-server
wget -N -q --directory-prefix ${SELENIUM_WORKSPACE} http://selenium-release.storage.googleapis.com/${SELENIUM_VERSION_SHORT}/selenium-server-standalone-${SELENIUM_VERSION_LONG}.jar

# remove symlink (from last installation)
rm -f ${SELENIUM_WORKSPACE}/selenium-server-standalone-latest.jar

# create symlink
cp ${SELENIUM_WORKSPACE}/selenium-server-standalone-${SELENIUM_VERSION_LONG}.jar ${SELENIUM_WORKSPACE}/selenium-server-standalone-latest.jar

# delete all cronjobs to make no cron job is installed twice
crontab -r

# install cron to restart eth0 when network is broken (stability)
sudo rm -f /selenium-workspace/cron_check_eth0.sh
sudo cp ${THIS_SCRIPT_PATH}/resources/cron_check_eth0.sh /selenium-workspace/cron_check_eth0.sh
sudo chmod +x /selenium-workspace/cron_check_eth0.sh
(crontab -l ; echo '* * * * * sudo sh /selenium-workspace/cron_check_eth0.sh') | crontab
